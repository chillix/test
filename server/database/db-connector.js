const MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://localhost:27017/auctionwars2', (err, conn) => {
    if (err) {
        console.log('Failed to connect to database', err);
    }
    db = conn;
});

function getCollection(collectionName) {
      return db.getCollection(collectionName)
}

module.exports = getCollection