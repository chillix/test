var connector = require('./db-connector')

const collection = connector('prices')

function deleteAllPrices() {
    collection.remove({})
}

function deletePricesInPeriod(start, end) {
    collection.remove({
        'timestamp': {
            $gte: start,
            $lt: end
        }
    })
}

function insertPricesWithTimestamp(prices, timestamp) {
    return new Promise((resolve, reject) => {
        collection.insert({'timestamp': timestamp, 'prices': prices}, (err, items) => {
            if (err) {
                reject(err)
            } else {
                resolve(items.length)
            }
        })
    })
}

function insertPrices(prices) {
    return insertPricesWithTimestamp(prices, new Date())
}

function saveInsertPricesWithTimestamp(prices, timestamp) {
    return new Promise((resolve, reject) => {
            collection.update({'timestamp': timestamp}, {$pushAll: {'prices': prices}}, {upsert: true}, (err, items) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(items.length)
                }
            })  
    })
}

function saveInsertPrices(prices) {
    return saveInsertPricesWithTimestamp(prices, new Date())
}

function findAllPrices() {
    return new Promise((resolve, reject) => {
        collection.find({}).toArray((err, items) => {
            if (err) {
                reject(err)
            } else {
                resolve(items)
            }
        })
    })
}

function findPricesInPeriod(start, end) {
    return new Promise((resolve, reject) => {
        collection.find({
            'timestamp': {
                $gte: start,
                $lte: end
            }
        }).toArray((err, items) => {
            if (err) {
                reject(err)
            } else {
                resolve(items)
            }
        })
    })
}