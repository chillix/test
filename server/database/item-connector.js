var connector = require('./db-connector')

const collection = connector('items')

function deleteAllItems() {
    collection.remove({})
}

function deleteItems(ids) {
    if (ids.constructor === Array) {
        collection.remove({'id': {'$in': ids}})
    } else {
        collection.remove({'id': ids})
    }
}

function insertItems(items) {
    return new Promise((resolve, reject) => {
        collection.insert(prices, (err, docs) => {
            if (err) {
                reject(err)
            } else {
                resolve(docs.length)
            }
        })
    })
}

function findAllItems() {
    return new Promise((resolve, reject) => {
        collection.find({}).toArray((err, docs) => {
            if (err) {
                reject(err)
            } else {
                resolve(docs)
            }
        })
    })
}

function getItems(ids) {
    if (ids.constructor === Array) {
        return getItemsByIds(ids)
    } else {
        return getItemById(ids)
    }
}

function getItemsByIds(ids) {
    collection.find({'id': {'$in': ids}}).toArray((err, docs) => {
        if (err) {
            reject(err)
        } else {
            resolve(docs)
        }
    })
}

function getItemById(id) {
    collection.find({'id': id}).toArray((err, docs) => {
        if (err) {
            reject(err)
        } else {
            if (docs.length == 0) {
                return {}
            } else {
                return docs[0]
            }
        }
    })
}



