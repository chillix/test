var express = require('express');
var router = express.Router();
var PriceImport = require('../jobs/price-import');

router.get('/triggerFullImport', (request, response) => {
    PriceImport(request.db.collection('prices'), request.db.collection('items'), new Date()).then(data => {
        console.log('Finished price import')
    }).catch(error => {
        console.log('Failed to import prices', error)
    })
    response.send('Price import started. Doing some heavy lifting now')
})

router.get('/fullPrices', (request, response) => {
    request.db.collection('prices').find({}).toArray((error, results) => {
        if (error) {
            response.send(error)
        } else {
            response.json(results)
        }
    })
})

router.get('/clean', (request, response) => {
    request.db.collection('prices').remove({})
    response.send('Removed all loaded prices')
})

module.exports = router