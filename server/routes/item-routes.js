var express = require('express');
var router = express.Router();
var PriceImport = require('../jobs/price-import');

router.get('/triggerFullImport', (request, response) => {
    ItemImport(request.db.collection('items')).then(data => {
        console.log('Finished item import')
    }).catch(error => {
        console.log('Failed to import items', error)
    })
    response.send('Item import started. Doing some heavy lifting now')
})

router.get('/count', (request, response) => {
    request.db.collection('items').find({}, {"id": true}).toArray().then(data => {
        response.json(data.length)
    }).catch(error => {
        response.sendStatus(500)
    })
})

router.get('/ids', (request, response) => {
    request.db.collection('items').find({}, {"id": true}).toArray().then(data => {
        response.json(data)
    }).catch(error => {
        response.sendStatus(500)
    })
})

router.get('/id/:itemId', (request, response) => {
    request.db.collection('items').findOne({"id": request.params.itemId}, (err, res) => {
        if (err) {
            response.sendStatus(404)
        } else {
            response.json(res)
        }
    })
})

router.get('/all', (request, response) => {
    request.db.collection('items').find({}).toArray().then(data => {
        response.json(data)
    }).catch(error => {
        response.sendStatus(500)
    })
})

module.exports = router;