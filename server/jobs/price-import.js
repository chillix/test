const https = require('https')
const logger = require('winston')

logger.add(logger.transports.File, { filename: 'item-import.log' })
logger.remove(logger.transports.Console)

function importPrices(priceCollection, itemCollection, timestamp) {
    const ids = gatherIds(itemCollection)
    const chunkedIds = chunkArray(ids, 200)
    const uris = computeUrisFromids(chunkedIds)
    return runAllPriceImports(uris, priceCollection, timestamp)
}

function gatherIds(collection) {
    return collection.find({}, {"id": true}).toArray()
}

function chunkArray(array, chunkSize) {
    return new Promise((resolve, reject) => {
        array.then(data => {
            var chunks = []
            while (data.length > 0) {
                chunks.push(data.splice(0, chunkSize))
            }
            resolve(chunks)
        })
    })
}

function computeUrisFromids(idsPromise) {
    return new Promise((resolve, reject) => {
        idsPromise.then(data => {
            var uris = []
            for (var i = 0; i < data.length; i++) {
                var baseString = 'https://api.guildwars2.com/v2/commerce/prices?ids='
                let ids = data[i]
                for (var j = 0; j < ids.length; j++) {
                    baseString += ids[j].id
                    baseString += ','
                }
                baseString = baseString.slice(0, -1)
                uris.push(baseString)
            }
            resolve(uris)
        })
    })
} 

function createImportCall(uri) {
    return () => {
        return new Promise((resolve, reject) => {
            https.get(uri, response => {
                response.setEncoding('utf8')
                content = ''

                response.on('data', d => {
                    content += d
                })

                response.on('end', () => {
                    resolve(JSON.parse(content))
                })
            }).on('error', err => {
                reject(err)
            })
        })
    }
}

function runAllPriceImports(urisPromise, collection, timestamp) {
    return new Promise((resolve, reject) => {
        urisPromise.then(uris => {
            var allJobs = uris.map(uri => createImportCall(uri))
            var currentCall = 1;
            logger.info('Import is starting now, will make ' + allJobs.length + ' calls')
            var allErrors = []
            var next = () => {
                if (allJobs.length === 0) {
                    logger.info('Import done, made ' + currentCall + ' calls with ' + allErrors.length + ' errors')
                    resolve()
                } else {
                    const job = allJobs.splice(0, 1)[0]
                    logger.info('Making call no ' + currentCall++)
                    job().then(prices => {
                        processPrices(prices, collection, timestamp)
                        next()
                    }).catch(error => {
                        allErrors.push(error)
                    })
                }
            }
            next()
        }).catch(err => {
            logger.error('Aborting import due to promise error ', err)
            reject(err)
        })
    })
}

function processPrices(prices, collection, timestamp) {
    var processedPrices = []
    for (var i = 0; i < prices.length; i++) {
        prices[i].profit = parseInt(prices[i].sells.unit_price * 0.85 - prices[i].buys.unit_price)
        processedPrices.push(prices[i])
    }
    savePrices(processedPrices, collection, timestamp)
}

function savePrices(processedPrices, collection, time) {
    collection.update({"timestamp": time}, {$pushAll: {"prices": processedPrices}}, {upsert: true})  
}

module.exports = importPrices