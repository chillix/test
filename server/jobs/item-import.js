const https = require('https')
const winston = require('winston')

winston.add(winston.transports.File, { filename: 'item-import.log' });
winston.remove(winston.transports.Console);

function importItems(collection) {
    const localIds = fetchItemIdsFromLocalDb(collection)
    const apiIds = fetchItemIdsFromApi()
    const idsToImport = retainAllIds(apiIds, localIds)
    const chunkedIds = chunkArray(idsToImport, 200)
    const importUris = generateUrisFromChunkedIds(chunkedIds)
    const allItems = runAllItemImports(importUris)
    return saveDataToCollection(collection, allItems)
}

function fetchItemIdsFromLocalDb(collection) {
    return collection.find({}, {"id": true}).toArray()
}

function fetchItemIdsFromApi() {
    return new Promise((resolve, reject) => {
        https.get('https://api.guildwars2.com/v2/items', response => {
            var content = ''
            response.setEncoding('utf8')
            response.on('data', data => {
                content += data
            })
            response.on('end', () => {
                resolve(JSON.parse(content))
            })
        }).on('error', error => {
            reject(error)
        })
    })
}

function retainAllIds(baseIds, subtractIds) {
    return new Promise((resolve, reject) => {
        Promise.all([baseIds, subtractIds])
        .then(data => {
            retainedIds = data[0].filter(x => data[1].indexOf(x) == -1)
            winston.info('Items to import: ' + retainedIds.length)
            resolve(retainedIds)
        }).catch(error => {
            winston.error('Got an error when trying to calculate ids to import', error)
            reject(error)
        })
    })
}

function chunkArray(array, chunkSize) {
    return new Promise((resolve, reject) => {
        array.then(data => {
            var chunks = []
            while (data.length > 0) {
                chunks.push(data.splice(0, chunkSize))
            }
            winston.info('Chunking ids is done, resulting calls: ' + chunks.length)
            resolve(chunks)
        })
    })
}

function generateUrisFromChunkedIds(chunkedIds) {
    return new Promise((resolve, reject) => {
        chunkedIds.then(data => {
            var uris = []
            for (var i = 0; i < data.length; i++) {
                var baseString = 'https://api.guildwars2.com/v2/items?ids='
                let ids = data[i]
                for (var j = 0; j < ids.length; j++) {
                    baseString += ids[j]
                    baseString += ','
                }
                baseString = baseString.slice(0, -1);
                uris.push(baseString)
            }
            resolve(uris)
        })
    })
}

function createItemImportCall(uri) {
    return () => {
        return new Promise((resolve, reject) => {
            https.get(uri, response => {
                var content = ''
                response.setEncoding('utf8')
                response.on('data', data => {
                    content += data
                })
                response.on('end', () => {
                    resolve(JSON.parse(content))
                })
            }).on('error', error => {
                reject(error)
            })
        })
    }
}

function runAllItemImports(uris) {
    return new Promise((resolve, reject) => {
        uris.then(data => {
            var allJobs = data.map(uri => createItemImportCall(uri))
            const roundsTotal = allJobs.length
            var round = 1;
            var allErrors = []
            var allItems = []
            var next = () => {
                if (allJobs.length === 0) {
                    winston.info('Done performing calls, resulted in ' + allErrors.length + ' errors')
                    winston.info('Imported ' + allItems.count + ' new items')
                    resolve(allItems)
                } else {
                    winston.verbose('Performing call ' + round++ + ' of ' + roundsTotal)
                    const job = allJobs.splice(0, 1)[0]
                    job().then(d => {
                        allItems = allItems.concat(d)
                        next()
                    }).catch(error => {
                        allErrors.push(error)
                        next()
                    })
                }
            }
            next()
        }).catch(error => {
            reject(error)
        })
    })
}

function saveDataToCollection(collection, data) {
    return new Promise((resolve, reject) => {
        data.then(items => {
            collection.insert(items, (error, records) => {
                resolve(records.length)
            })
        }).catch(error => {
            reject(error)
        })
    })
}

module.exports = importItems