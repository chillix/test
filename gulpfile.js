var gulp = require('gulp');
var gls = require('gulp-live-server');

gulp.task('serve', function () {
    var server = gls.new('./index.js');
    server.start();
    
    gulp.watch(
        [
            "./server/**/*.js"
        ],
        function (file) {
            process.stdout.write('reloaded server files\n');
            server.notify(file);
        });

    gulp.watch("./index.js", function (file) {
        server.start();
        process.stdout.write('reloaded server files\n');
        server.notify(file);
    });
});

