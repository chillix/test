var express = require('express');
var app = express();
var itemRoutes = require('./server/routes/item-routes');
var priceRoutes = require('./server/routes/price-routes');
var MongoClient = require('mongodb').MongoClient;

var db;

MongoClient.connect('mongodb://localhost:27017/auctionwars2', (err, conn) => {
    if (err) {
        console.log('Failed to connect to database', err);
    }
    db = conn;
});

app.use((request, response, next) => {
    request.db = db;
    next();
})

app.use('/item', itemRoutes);
app.use('/price', priceRoutes);

app.listen(3000);